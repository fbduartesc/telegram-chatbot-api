<div align="center">
  <img alt="Chatbot" src="https://gitlab.com/fbduartesc/chatbot-page/-/raw/master/images/chatbot.png" height="400px" />    
  <h1>Express | Axios | NodeJS | Javascript</h1>

 :rocket: *Chatbot page: this project will build a bot for telegram using javascript, express, axios and nodejs*
  </div>

# :robot: Chatbot page
Demo: [Chatbot page](https://fbduartesc.gitlab.io/chatbot-page).  
Chatbot page: [Chatbot project](https://gitlab.com/fbduartesc/chatbot-page)

# :construction_worker: How to run

### :computer: Instructions 
```bash
# Rename the file .env.sample to .env
# Change the variables this file

# TOKEN=<YOUR_TOKEN>
# IDSUPER=<YOUR_ID_USER>
# USERID=<YOUR_ID_USER>
# URL_API=https://api.telegram.org/bot${TOKEN}
# FILE_URL_API=https://api.telegram.org/file/bot${TOKEN}
```

### :computer: Downloading project 

```bash
# Clone repository into your machine
$ git clone https://gitlab.com/fbduartesc/telegram-chatbot-api.git
```

### :computer: Installation 

```bash
# Install dependencies
$ npm install
```

### 💻 Running project on a web browser

```bash
# Run the command, to start the application
$ npm run dev
```

# :closed_book: License

Released in 2020.

Made with passion by [Fabio Duarte de Souza](https://gitlab.com/fbduartesc) 🚀.
This project is under the [MIT license](https://gitlab.com/fbduartesc/chatbot-page/blob/master/LICENSE).