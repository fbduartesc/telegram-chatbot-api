const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');
const express = require('express');
const app = express();
const axios = require('axios');
const myEnv = dotenv.config();
dotenvExpand(myEnv);
const bodyParser = require('body-parser');
const cors = require('cors');

const listOptions = {
    INSTAGRAM: {
        title: 'Login page instagram',
        url: 'https://fbduartesc.gitlab.io/login-page-instagram'
    },
    NETFLIX: {
        title: 'Netflix clone',
        url: 'https://fbduartesc.gitlab.io/netflix-clone '
    },
    RESCUE: {
        title: 'Rescue game',
        url: 'https://fbduartesc.gitlab.io/rescue-game-javascript'
    },
    GENESIS: {
        title: 'Genesis game',
        url: 'https://fbduartesc.gitlab.io/genesis-game'
    },
    DINO: {
        title: 'Dino game',
        url: 'https://fbduartesc.gitlab.io/dino-game'
    },
    MEMORY: {
        title: 'Memory game',
        url: 'https://fbduartesc.gitlab.io/memory-game-javascript'
    },
    HASH: {
        title: 'Hash game',
        url: 'https://fbduartesc.gitlab.io/hash-game-javascript'
    },
    SPACE: {
        title: 'Space shooter',
        url: 'https://fbduartesc.gitlab.io/space-shooter-javascript'
    }
};

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/', async (req, res, next) => {
    const chatId = process.env.USERID;
    const sentMessage = req.body.message;

    if (sentMessage.match(/clone|clones|instagram|netflix/gi)) {
        const opcoes = getOptions();
        const result = await enviarMensagem(opcoes, chatId);
        return res.status(200).json({ message: result.data.result.text })
    } if (sentMessage.match(/games|javascript|space|shooter|hash|memory|dino|genesis|rescue/gi)) {
        const opcoes = getOptionsGames();
        const result = await enviarMensagem(opcoes, chatId);
        return res.status(200).json({ message: result.data.result.text })
    } else {
        res.status(200).send({ message: 'Não entendi o que você quer. Pode repetir por favor!' });
    }
});

const enviarMensagem = (opcoes, chatId) => {
    return axios.post(`${process.env.URL_API}/sendMessage`,
        {
            chat_id: chatId,
            text: `${opcoes}`
        });
};

function getOptions() {
    const option = `
        <ul>
            <li>                
                <a href="${listOptions.INSTAGRAM.url}" target="_blank">                    
                    <i class="fab fa-instagram"></i>
                    ${listOptions.INSTAGRAM.title}
                </a>
            </li>
            <li>
                <a href="${listOptions.NETFLIX.url}" target="_blank">
                    <i class="fa fas fa-tv"></i>
                    ${listOptions.NETFLIX.title}
                </a>
            </li>
        </ul>
    `;
    return option;
}

function getOptionsGames() {
    const option = `
        <ul>
            <li>
                <a href="${listOptions.RESCUE.url}" target="_blank">
                    <i class="fa fas fa-helicopter"></i>
                    ${listOptions.RESCUE.title}</a>
            </li>
            <li>
                <a href="${listOptions.GENESIS.url}" target="_blank">
                    <i class="fa fas fa-gamepad"></i>
                    ${listOptions.GENESIS.title}
                </a>
            </li>
            <li>
                <a href="${listOptions.DINO.url}" target="_blank">
                    <i class="fa fas fa-dragon"></i>
                    ${listOptions.DINO.title}
                </a>
            </li>
            <li>
                <a href="${listOptions.MEMORY.url}" target="_blank">
                    <i class="fa fas fa-memory"></i>
                    ${listOptions.MEMORY.title}
                </a>
            </li>
            <li>
                <a href="${listOptions.HASH.url}" target="_blank">
                    <i class="fa fas fa-hashtag"></i>
                    ${listOptions.HASH.title}
                </a>
            </li>
            <li>
                <a href="${listOptions.SPACE.url}" target="_blank">
                    <i class="fa fas fa-space-shuttle"></i>
                    ${listOptions.SPACE.title}
                </a>
            </li>
        </ul>
    `;
    return option;
}

const port = process.env.PORT || 5000;
app.listen(port);
console.log(`Server API in ${port}`);